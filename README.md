# gengen-cli

CLI to predict the gender of names.

## Installation 
 
```bash
git clone https://gitlab.com/pewen/blog-tools/gengen-cli.git
cd gengen-cli
pip install -r requirements.txt
```

## Usage

```
Usage: gengen.py [OPTIONS] INPUT

  Predicts gender of the INPUT.

  If --header is used, skip the first line, and if --output is used, write
  the predicts gender to a file.

Options:
  --header                        If the input has a header in the first line.
  --output PATH                   Output path. Overwrite the file if exists.
```

### Note

You can access the fasttext model directly in `model/model`.
