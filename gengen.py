import csv
from pathlib import Path

import typer
import fasttext
from unidecode import unidecode


model_path = "model/model"


def process_name(name: str) -> str:
    return unidecode(name.lower())


class Model:
    def __init__(self, model_path):
        self.model = fasttext.load_model(model_path)

    def predict_gender(self, raw_name: str) -> str:
        name = process_name(raw_name)
        gender, prob = self.model.predict(name)
        return gender[0][-1]


def main(
    input: Path,  # = typer.Option(..., "--input", help="Path to csv with the names.", exists=True, file_okay=True, readable=True),
    header: bool = typer.Option(
        False, "--header", help="If the input has a header in the first line."
    ),
    output: Path = typer.Option(
        None, "--output", help="Output path. Overwrite the file if exists."
    ),
):
    """
    Predicts gender of the INPUT.

    If --header is used, skip the first line, and if --output is used, write the predicts gender to a file.
    """
    model = Model(model_path)

    with input.open() as f:
        if header:
            _ = f.readline()

        if output:
            f_out = output.open("w")
            writer = csv.writer(f_out, quoting=csv.QUOTE_MINIMAL)
            if header:
                writer.writerow(["name", "gender"])

        for _, raw_name in enumerate(f):
            raw_name = raw_name.strip()
            gender = model.predict_gender(raw_name)

            if output:
                writer.writerow([raw_name, gender])
            else:
                typer.echo(f"{raw_name} -> {gender}")

        if output:
            f_out.close()


if __name__ == "__main__":
    typer.run(main)
